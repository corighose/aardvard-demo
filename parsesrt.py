#!/usr/bin/env python3

import logging
import sys


logging.basicConfig(level = logging.DEBUG, format = '%(levelname)s %(message)s')


def read_args():
    fname = sys.argv[1]
    logging.debug('File to load: ' + fname)
    return fname


def load_text(fname):
    logging.debug('Loading text...')
    with open(fname) as f:
        data = f.read()
    logging.debug('Done')
    return data


def delete_unnecessary(text):
    logging.debug('Deleting unnecessary lines')
    text = '\n' + text
    lines = text.split('\n')
    i = 0
    for line in lines:
        trigger = '-->' in line
        if trigger:
            lines[i-2] = '\n'
            lines[i-1:i+1] = []
            lines[i-1:i+1] = []
            lines[i-1:i+1] = []
        i += 1

    new_text = '\n'.join(lines)

    new_text = new_text.replace('\n\n', '\n')
    return new_text


def delete_tags(text):
    logging.debug('Deleting tags')
    symbols = []
    trigger = True
    for s in text:
        if s == '<':
            trigger = False

        if s == '>':
            trigger = True

        if trigger:
            symbols.append(s)

    symbols = list(filter(('>').__ne__, symbols))
    new_text = "".join(symbols)
    return new_text


def parse(inputFile):
    formatted = delete_tags(delete_unnecessary(load_text(inputFile)))
    #print(formatted)
    return formatted


#main()